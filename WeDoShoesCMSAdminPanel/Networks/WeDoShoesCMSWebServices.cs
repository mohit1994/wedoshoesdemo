﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.Networks
{
    public static class WeDoShoesCMSWebServices
    {
        #region BASE URL
        //public const string BASE_URI = "https://prod-api-cms.wedoshoes.com:8443/api/v1"; // Just for internal testing        
        public const string BASE_URI = "https://stage-api-cms.wedoshoes.com:8443/api/v1"; // For Staging With APP        
        #endregion        

        #region CATEGORY
        public const string LIST_OF_CATEGORIES = "{0}/products-categories?minimal_info=false";
        public const string LIST_OF_CATEGORIES_MIN_DETAILS = "{0}/products-categories?minimal_info=true";
        public const string CATEGORY = "{0}/products-categories/{1}";
        public const string CREATE_CATEGORY = "{0}/products-categories";
        public const string UPDATE_CATEGORY = "{0}/products-categories/{1}";
        public const string DELETE_CATEGORY = "{0}/products-categories/{1}";
        #endregion

        #region SERVICE
        public const string LIST_OF_ROOT_SERVICE = "{0}/wedo-services?offset=0&limit=100&service_type=ROOT&minimal_info=false";
        public const string ROOT_SERVICE_BY_CATEGORY_ID = "{0}/wedo-services?offset={1}&limit=100&service_type=ROOT&category_id={2}&minimal_info=false";
        public const string SERVICE_BY_CATEGORY_AND_TYPE = "{0}/wedo-services?offset=0&limit=100&service_type={1}&category_id={2}&minimal_info=true";
        public const string SERVIDE_BY_PRODUCT = "{0}/products/{1}/services";
        public const string ROOT_SERVICE_BY_CATEGORY_ID_MINIMAL_INFO = "{0}/wedo-services?offset=0&limit=100&service_type=ROOT&category_id={1}&minimal_info=true";
        public const string SERVICE_BY_ID = "{0}/wedo-services/{1}";
        public const string CREATE_ROOT_SERVICE = "{0}/wedo-services";
        public const string CREATE_SERVICE = "{0}/wedo-services";
        public const string CREATE_SUB_SERVICE = "{0}/wedo-services";
        public const string UPDATE_ROOT_SERVICE = "{0}/wedo-services/{1}";
        public const string UPDATE_SERVICE = "{0}/wedo-services/{1}";
        public const string ROOT_SERVICE_BY_SERVICE_ID = "{0}/wedo-services/{1}";
        public const string DELETE_ROOT_SERVICE = "{0}/wedo-services/{1}";
        public const string SERVICE_BY_ROOT = "{0}/wedo-services?offset={1}&limit=20&parent_service_id={2}&minimal_info=false";
        public const string SUB_SERVICE_BY_PARENT_ID = "{0}/wedo-services?offset={1}&limit=100&parent_service_id={2}&minimal_info=false";
        public const string SUB_SERVICE_BY_ID = "{0}/wedo-services/{1}";
        public const string UPDATE_PROCESSING_TIME = "{0}/wedo-services/{1}/processing-time";
        public const string UPDATE_CHARGES = "{0}/wedo-services/{1}/convenience-charges";
        #endregion

        #region Accessories
        public const string LIST_OF_ACCESSORIES = "{0}/accessories?offset={1}&limit=20";
        public const string ACCESSORIES_BY_CATEGORY_ID = "{0}/accessories?offset={1}&limit=20&category_id={2}";
        public const string ACCESSORY_MIN_INFO_BY_CATEGORY = "{0}/accessories?offset=0&limit=100&category_id={1}&minimal_info=true";
        public const string GET_ACCESSORY_BY_ID = "{0}/accessories/{1}";
        public const string UPDATE_ACCESSORY = "{0}/accessories/{1}";        
        public const string CREATE_ACCESSORY = "{0}/accessories";
        public const string DELETE_ACCESSORY = "{0}/accessories/{1}";
        public const string UPDATE_PRICE = "{0}/accessories/{1}/price";
        #endregion

        #region Brands
        public const string LIST_OF_BRANDS = "{0}/products-brands";
        public const string LIST_OF_BRANDS_BY_CATEGORY = "{0}/products-brands?category_id={1}";
        public const string GET_BRAND_BY_ID = "{0}/products-brands/{1}";
        public const string CREATE_BRAND = "{0}/products-brands";
        public const string UPDATE_BRAND = "{0}/products-brands/{1}";
        public const string DELETE_BRAND = "{0}/products-brands/{1}";
        #endregion

        #region Products
        public const string LIST_PRODUCT = "{0}/products?limit=100&offset=0";
        public const string PRODUCT_BY_CATEGORY_ID = "{0}/products?category_id={1}&limit=20&offset={2}";
        public const string SIZE_BY_PRODUCT = "{0}/products/{1}/sizes";
        public const string BRAND_BY_PRODUCT = "{0}/products/{1}/brands";
        public const string SERVICE_BY_PRODUCT = "{0}/products/{1}/services";
        public const string ADD_SERVICE_TO_PRODUCT = "{0}/products/{1}/services";
        public const string ADD_BRAND_TO_PRODUCT = "{0}/products/{1}/brands";
        public const string REMOVE_BRAND_FROM_PRODUCT = "{0}/products/{1}/brands/{2}";
        public const string REMOVE_SIZE_FROM_PRODUCT = "{0}/products/{1}/sizes/{2}";
        public const string PRODUCT_BY_ID = "{0}/products/{1}";
        public const string CREATE_PRODUCT = "{0}/products";
        public const string UPDATE_PRODUCT = "{0}/products/{1}";
        public const string UPDATE_SERVICE_PRICE = "{0}/products/{1}/services/{2}/prices";
        #endregion

        #region SizeType
        public const string LIST_SIZE_TYPE = "{0}/products-size-types";
        public const string SIZE_TYPE_BY_ID = "{0}/products-size-types/{1}";
        public const string CREATE_SIZE_TYPE = "{0}/products-size-types";
        public const string UPDATE_SIZE_TYPE_BY_ID = "{0}/products-size-types/{1}";
        public const string UPDATE_COST_BY_SIZE_TYPE_ID = "{0}/accessories/{1}/price";
        #endregion

        #region ContactMode
        public const string CREATE_CONTACT_MODE  = "{0}/contact-modes";
        public const string UPDATE_CONTACT_MODE = "{0}/contact-modes/{1}";
        public const string CONTACT_MODE_BY_ID = "{0}/contact-modes/{1}";
        public const string LIST_CONTACT_MODE = "{0}/contact-modes?limit=10&offset=0";
        public const string DELETE_CONTACT_MODE = "{0}/contact-modes/{1}";
        #endregion

        #region Faq
        public const string CREATE_FAQ_CATEGORY = "{0}/faqs/categories";
        public const string UPDATE_FAQ_CATEGORY = "{0}/faqs/categories/{1}";
        public const string DELETE_FAQ_CATEGORY = "{0}/faqs/categories/{1}";
        public const string FAQ_CATEGORY_BY_ID = "{0}/faqs/categories/{1}";
        public const string LIST_FAQ_CATEGORY = "{0}/faqs/categories";
        public const string CREATE_FAQ = "{0}/faqs";
        public const string UPDATE_FAQ = "{0}/faqs/{1}";
        public const string DELETE_FAQ = "{0}/faqs/{1}";
        public const string LIST_FAQ = "{0}/faqs?limit=20&offset=0&category_id={1}";
        #endregion

        #region Tax
        public const string CREATE_Tax = "{0}/taxes";
        public const string UPDATE_Tax = "{0}/taxes";
        public const string DELETE_Tax = "{0}/taxes";
        public const string GET_Tax = "{0}/taxes";
        #endregion

        #region TimeSlot
        public const string CREATE_Time_Slot = "{0}/time-slots";
        public const string UPDATE_Time_Slot = "{0}/time-slots/{1}";
        public const string Time_Slot_BY_ID = "{0}/time-slots/{1}";
        public const string LIST_Time_Slot = "{0}/time-slots";
        public const string DELETE_Time_Slot = "{0}/time-slots/{1}";
        #endregion

        #region Defect
        public const string CREATE_DEFECT = "{0}/defects";
        public const string UPDATE_DEFECT = "{0}/defects/{1}";
        public const string DEFECT_BY_ID = "{0}/defects/{1}";
        public const string LIST_DEFECT = "{0}/defects";
        public const string DEFECT_BY_NAME = "{0}/defects?name={1}";
        #endregion

        #region NationState
        public const string CREATE_NATION_STATE = "{0}/nation-states";
        public const string UPDATE_NATION_STATE = "{0}/nation-states/{1}";
        public const string NATION_STATE_BY_ID = "{0}/nation-states/{1}";
        public const string LIST_NATION_STATE = "{0}/nation-states";
        public const string CREATE_DISTRICTS = "{0}/nation-states/{1}/districts";
        public const string UPDATE_DISTRICTS = "{0}/nation-states/{1}/districts/{2}";
        public const string STATE_DISTRICTS_BY_ID = "{0}/nation-states/{1}/districts/{2}";
        public const string LIST_STATE_DISTRICTS = "{0}/nation-states/{1}/districts";
        #endregion
    }
}