﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.Networks
{
    public class WeDoShoesCoreWebServices
    {
        #region BASE_URL
        //public const string BASE_URI = "https://prod-api-core.wedoshoes.com:8443/api/v1"; // Core Dev server base url
        public const string BASE_URI = "https://stage-api-core.wedoshoes.com:8443/api/v1"; // For Staging
        #endregion

        #region LOGIN
        public const string LOGIN = "{0}/admin/login";
        #endregion

        #region UserType
        public const string LIST_USER_TYPE = "{0}/user-types?offset=0&limit=20";
        public const string CREATE_USER_TYPE = "{0}/user-types";
        public const string GET_USER_TYPE = "{0}/user-types/{1}";
        public const string UPDATE_USER_TYPE = "{0}/user-types/{1}";
        public const string DELETE_USER_TYPE = "{0}/user-types/{1}";
        public const string LIST_USER_TYPE_BY_API = "{0}/user-types?offset={1}&limit=20&has_permission=true&api={2}";
        public const string LIST_USER_TYPE_NOT_IN_API = "{0}/user-types?offset={1}&limit=20&has_permission=false&api={2}";
        public const string ADD_USER_TYPE_IN_API = "{0}/apis/{1}/user-types";
        public const string REVOKE_USER_PERMISSION = "{0}/user-types/{1}/apis/{2}";
        #endregion

        #region API
        public const string LIST_API = "{0}/apis?offset={1}&limit=20";
        public const string LIST_API_BY_CATEGORY = "{0}/apis?offset={1}&limit=20&category={2}";
        public const string API_DETAILS = "{0}/apis/{1}";
        public const string LIST_API_CATEGORY = "{0}/apis/categories";
        public const string CREATE_API = "{0}/apis";
        public const string UPDATE_API = "{0}/apis/{1}";
        public const string LIST_API_BY_USER_TYPE = "{0}/apis?offset={1}&limit=20&has_permission=true&user_type={2}";
        public const string LIST_API_NOT_IN_USER_TYPE = "{0}/apis?offset={1}&limit=500&has_permission=false&user_type={2}";
        public const string ADD_API_IN_USER_TYPE = "{0}/user-types/{1}/apis";
        public const string REVOKE_API_PERMISSION = "{0}/apis/{1}/user-types/{2}";
        #endregion
    }
}