﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.Models
{
    public class Product
    {
        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public Int32? Id { get; set; }
        [JsonProperty(PropertyName = "category_id", NullValueHandling = NullValueHandling.Ignore)]
        public Int32? CategoryId { get; set; }
        [JsonProperty(PropertyName = "name", NullValueHandling = NullValueHandling.Ignore)]
        public String Name { get; set; }
        [JsonProperty(PropertyName = "description", NullValueHandling = NullValueHandling.Ignore)]
        public String Description { get; set; }
        [JsonProperty(PropertyName = "size_types", NullValueHandling = NullValueHandling.Ignore)]
        public List<int> SizeTypes { get; set; }        
        [JsonProperty(PropertyName = "services", NullValueHandling = NullValueHandling.Ignore)]
        public List<WeDoShoesService> Services { get; set; }
        [JsonProperty(PropertyName = "brands", NullValueHandling = NullValueHandling.Ignore)]
        public List<int> Brands { get; set; }
        [JsonProperty(PropertyName = "image_url", NullValueHandling = NullValueHandling.Ignore)]
        public String ImageUrl { get; set; }

        public bool ShouldSerializeConvenienceCharges()
        {
            return (SizeTypes != null || Services != null || Brands != null);
        }
    }
}