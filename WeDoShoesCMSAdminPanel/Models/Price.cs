﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.Models
{    
    public class Price
    {
        public Price()
        {
            Accessory = new Accessory();
        }
        [JsonProperty(PropertyName = "product_size_type", NullValueHandling = NullValueHandling.Ignore)]
        public int? ProductSizeType { get; set; }
        [JsonProperty(PropertyName = "labor_cost", NullValueHandling = NullValueHandling.Ignore)]        
        public Decimal? LabourCost { get; set; }
        [JsonProperty(PropertyName = "material_cost", NullValueHandling = NullValueHandling.Ignore)]
        public Decimal? MaterialCost { get; set; }
        [JsonProperty(PropertyName = "accessory", NullValueHandling = NullValueHandling.Ignore)]
        public Accessory Accessory { get; set; }
        [JsonProperty(PropertyName = "profit", NullValueHandling = NullValueHandling.Ignore)]
        public Decimal? Profit { get; set; }

        public bool ShouldSerializeAccessory()
        {
            return (Accessory.Id != null);
        }
    }
}