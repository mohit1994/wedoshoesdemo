﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.ViewModel
{
    public class PriceView
    {
        public PriceView()
        {
            ListAccessoryView = new List<AccessoryView>();
            AccessoryView = new AccessoryView();
        }
        public int? ProductSizeType { get; set; }
        [Display(Name="Size")]
        public string ProductSizeTypeName { get; set; }
        [Required(ErrorMessage="Labour cost is Required"), Display(Name="Labour Cost")]
        public Decimal? LabourCost { get; set; }
        [Required(ErrorMessage = "Material cost is Required"), Display(Name = "Material Cost")]
        public Decimal? MaterialCost { get; set; }
        [Required(ErrorMessage="Accessory cost is Required"), Display(Name="Accessory")]
        public AccessoryView AccessoryView { get; set; }
        public List<AccessoryView> ListAccessoryView { get; set; }
        [Required(ErrorMessage="Profit is Required"), Display(Name="Profit")]
        public Decimal? Profit { get; set; }
    }
}