﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeDoShoesCMSAdminPanel.ViewModel
{
    public class TaxView
    {
        [Display(Name = "Tax Id")]
        public int Id { get; set; }
        [Required(ErrorMessage = "TaxRate is Required"), Display(Name = "Tax Rate")]
        public double TaxRate { get; set; }
    }
}
