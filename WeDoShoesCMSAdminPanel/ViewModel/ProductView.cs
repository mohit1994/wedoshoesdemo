﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.ViewModel
{
    public class ProductView
    {
        public ProductView()
        {
            ListServiceView = new List<ServiceView>();
            ServiceView = new ServiceView();
            SizeTypeView = new List<SizeTypeView>();
            ListBrandsView = new List<BrandsView>();
        }
        public Int32? Id { get; set; }
        [Required(ErrorMessage = "Category is Required"), Display(Name = "Category")]
        public Int32? CategoryId { get; set; }
        [Required(ErrorMessage = "Name is Required"), Display(Name = "Name")]
        public String Name { get; set; }
        [Display(Name = "Description")]
        public String Description { get; set; }
        [Display(Name = "Size")]
        public List<int> SelectedSizeIds { get; set; }
        [Required(ErrorMessage = "Service is Required"), Display(Name = "Service")]
        public List<ServiceView> ListServiceView { get; set; }
        [Display(Name = "Service")]
        public ServiceView ServiceView { get; set; }
        public Int32? SelectedServiceId { get; set; }
        public List<SizeTypeView> SizeTypeView { get; set; }
        [Required(ErrorMessage = "Brand is Required"), Display(Name = "Brand")]
        public List<int> SelectedBrandIds { get; set; }
        [Display(Name = "Image")]
        public String ImageUrl { get; set; }
        [Display(Name = "Brands")]
        public List<BrandsView> ListBrandsView { get; set; }
    }
}