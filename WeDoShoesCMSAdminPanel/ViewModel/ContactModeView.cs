﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeDoShoesCMSAdminPanel.ViewModel
{
    public class ContactModeView
    {
        [Display(Name = "Contact Mode")]
        public Int32 Id { get; set; }
        [Required(ErrorMessage = "Name is Required"), Display(Name = "Name")]
        public string Name { get; set; }
    }
}
