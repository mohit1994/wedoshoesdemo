﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WeDoShoesCMSAdminPanel.Filter;
using WeDoShoesCMSAdminPanel.Services;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Controllers
{
    [AuthorizeFilter]
    [RequestFilter]
    public class AccessoriesController : Controller
    {
        private readonly AccessoriesService _accessoriesService;
        private readonly Logger _logger;
        public AccessoriesController()
        {
            _accessoriesService = new AccessoriesService();
            _logger = LogManager.GetCurrentClassLogger();
        }
        // GET: Accessories
        public ActionResult Index()
        {
            try
            {
                List<AccessoryView> listAccessories = _accessoriesService.ListAccessories(0);
                return View(listAccessories);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: Assessories by category id.        
        public ActionResult ListByCategory(int categoryId)
        {
            try
            {
                List<AccessoryView> listAccessories = _accessoriesService.ListAccessoriesByCategoryId(categoryId, 0);
                ViewBag.categoryid = categoryId;
                return View(listAccessories);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Accessories/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                AccessoryView accessory = _accessoriesService.GetAccessoryById(id);
                return View(accessory);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Accessories/Create
        public ActionResult Create(int? categoryId)
        {
            if (categoryId != null)
            {
                AccessoryView accessory = new AccessoryView()
                {
                    CategoryId = categoryId
                };
                return View(accessory);
            }
            return View();
        }

        // POST: Accessories/Create
        [HttpPost]
        public ActionResult Create(AccessoryView request)
        {
            if (!ModelState.IsValid)
                return View(request);
            try
            {
                bool isCreated = _accessoriesService.CreateAccessory(request);
                if (isCreated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Accessories/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                AccessoryView accessory = _accessoriesService.GetAccessoryById(id);
                return View(accessory);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: Accessories/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, AccessoryView request)
        {            
            try
            {
                bool isUpated = _accessoriesService.UpdateAccessory(request);
                if (isUpated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult EditCost(int id, AccessoryView request)
        {
            if (request.AccessoryPriceView.AccessoryCost == null)
                return View("Edit", request);
            try
            {
                bool isUpated = _accessoriesService.UpdateCost(request);
                if (isUpated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Accessories/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                AccessoryView accessory = _accessoriesService.GetAccessoryById(id);
                return View(accessory);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: Accessories/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, AccessoryView request)
        {
            try
            {
                bool isDeleted = _accessoriesService.DeleteAccessory(request);
                if (isDeleted)
                    return RedirectToAction("Index");
                return RedirectToAction("Index");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult LoadMoreAccessory(int? categoryId, int size)
        {
            try
            {
                List<AccessoryView> listAccessories = _accessoriesService.ListAccessoriesByCategoryId(categoryId, size);
                if (listAccessories != null)
                {
                    return PartialView("_LoadMoreAccessory", listAccessories);                    
                }
                return Json(listAccessories);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //#region Helper Method
        //Convert html view to string.
        //public string RenderRazorViewToString(string viewName, object model)
        //{
        //    ViewData.Model = model;
        //    using (var sw = new StringWriter())
        //    {
        //        var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
        //                                                                 viewName);
        //        var viewContext = new ViewContext(ControllerContext, viewResult.View,
        //                                     ViewData, TempData, sw);
        //        viewResult.View.Render(viewContext, sw);
        //        viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
        //        return sw.GetStringBuilder().ToString();
        //    }
        //}
        //#endregion
    }
}
