﻿using NLog;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WeDoShoesCMSAdminPanel.Filter;
using WeDoShoesCMSAdminPanel.Services;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Controllers
{
    [AuthorizeFilter]
    public class DefectController : Controller
    {
        private readonly DefectService _defectService;
        private readonly Logger _logger;
        public DefectController()
        {
            _defectService = new DefectService();
            _logger = LogManager.GetCurrentClassLogger();
        }
        // GET: Defect
        public ActionResult Index()
        {
            try
            {
                List<DefectView> listDefectView = _defectService.ListDefectView();
                return View(listDefectView);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Defect/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                DefectView defect = _defectService.DefectById(id);
                return View(defect);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Defect/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Defect/Create
        [HttpPost]
        public ActionResult Create(DefectView request)
        {
            if (!ModelState.IsValid)
                return View();
            try
            {
                bool isCreated = _defectService.CreateDefect(request);
                if (isCreated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: Defect/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                DefectView defect = _defectService.DefectById(id);
                return View(defect);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: Defect/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, DefectView request)
        {
            if (!ModelState.IsValid)
                return View(request);
            try
            {
                bool isUpdated = _defectService.UpdateDefect(request);
                if (isUpdated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: Defect/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                DefectView defect = _defectService.DefectById(id);
                return View(defect);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: Defect/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, DefectView request)
        {
            try
            {
                bool isDeleted = _defectService.DeleteDefect(request);
                if (isDeleted)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: Defect/Search
        public ActionResult Search()
        {
            return View();
        }

        public ActionResult SearchDefect(string Keyword)
        {
            try
            {
            List<DefectView> listDefectView = _defectService.DefectByName(Keyword);
            return PartialView("_SearchDefect", listDefectView);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }
    }
}
