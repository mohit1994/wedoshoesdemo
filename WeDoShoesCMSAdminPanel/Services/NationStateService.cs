﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Util;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Services
{
    public class NationStateService
    {
        private readonly WeDoShoesCMSHttpRequester _weDoShoesCMSHttpRequester;
        private readonly Logger _logger;

        public NationStateService()
        {
            _weDoShoesCMSHttpRequester = new WeDoShoesCMSHttpRequester();
            _logger = LogManager.GetCurrentClassLogger();
        }

        internal List<NationStateView> ListNationState()
        {
            List<NationState> listNationState = _weDoShoesCMSHttpRequester.ListNationState();
            if (listNationState == null)
                return null;
            List<NationStateView> listNationStateView = new List<NationStateView>();
            foreach (var nationState in listNationState)
            {
                listNationStateView.Add(new NationStateView()
                {
                    Id = nationState.Id,
                    Name = nationState.Name
                });
            }

            return listNationStateView;
        }



        internal NationStateView NationStateById(int id)
        {
            NationState nationState = _weDoShoesCMSHttpRequester.NationStateDetails(id);
            NationStateView nationStateView = new NationStateView()
            {
                Id = nationState.Id,
                Name = nationState.Name
            };
            return nationStateView;
        }

        internal bool CreateNationState(NationStateView request)
        {
            NationState nationState = new NationState()
            {
                Name = request.Name
            };

            return _weDoShoesCMSHttpRequester.CreateNationState(nationState);
        }

        internal bool UpdateNationState(NationStateView request)
        {
            NationState nationState = new NationState()
            {
                Name = request.Name
            };

            return _weDoShoesCMSHttpRequester.UpdateNationState(request.Id, nationState);            
        }
    }
}
