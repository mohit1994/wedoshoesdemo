﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Networks;
using WeDoShoesCMSAdminPanel.Util;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Services
{
    public class BrandService
    {
        private readonly WeDoShoesCMSHttpRequester _weDoShoesCMSHttpRequester;
        private readonly Logger _logger;

        public BrandService()
        {
            _weDoShoesCMSHttpRequester = new WeDoShoesCMSHttpRequester();
            _logger = LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Get list of brands.
        /// </summary>
        /// <returns></returns>
        internal List<ViewModel.BrandsView> ListBrands()
        {
            List<Brands> listBrand = _weDoShoesCMSHttpRequester.ListBrands();
            if (listBrand == null)
                return null;
            List<BrandsView> listBrandView = new List<BrandsView>();
            foreach (var brand in listBrand)
            {
                listBrandView.Add(new BrandsView()
                    {
                        Id = brand.Id,
                        Name = brand.Name
                    });
            }
            return listBrandView;
        }

        /// <summary>
        /// Get brand by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal BrandsView GetBrandById(int id)
        {            
            Brands brand = _weDoShoesCMSHttpRequester.BrandDetails(id);
            BrandsView brandView = new BrandsView()
            {
                CategoryId = brand.CategoryId,
                Id = brand.Id,
                Name = brand.Name
            };
            return brandView;
        }

        /// <summary>
        /// Create brand.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool CreateBrand(BrandsView request)
        {            
            Brands brand = new Brands()
            {
                CategoryId = request.CategoryId,
                Name = request.Name
            };

            return _weDoShoesCMSHttpRequester.CreateBrand(brand);            
        }

        /// <summary>
        /// Update Brand By Id.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool UpdateBrand(BrandsView request)
        {            
            Brands brand = new Brands()
            {
                Name = request.Name
            };
            return _weDoShoesCMSHttpRequester.UpdateBrand(brand, request.Id);            
        }

        internal bool DeleteBrand(int id)
        {
            return _weDoShoesCMSHttpRequester.DeleteBrand(id);
        }

        /// <summary>
        /// Get brands by category
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        internal List<BrandsView> ListBrandsByCategory(int categoryId)
        {
            List<Brands> listBrand = _weDoShoesCMSHttpRequester.ListBrandsByCategory(categoryId);
            if (listBrand == null)
                return null;
            List<BrandsView> listBrandView = new List<BrandsView>();
            foreach (var brand in listBrand)
            {
                listBrandView.Add(new BrandsView()
                {
                    Id = brand.Id,
                    Name = brand.Name
                });
            }
            return listBrandView;
        }
    }
}