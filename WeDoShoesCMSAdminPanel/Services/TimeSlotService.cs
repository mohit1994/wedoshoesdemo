﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Util;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Services
{
    public class TimeSlotService
    {
        private readonly WeDoShoesCMSHttpRequester _weDoShoesCMSHttpRequester;
        private readonly Logger _logger;

        public TimeSlotService()
        {
            _weDoShoesCMSHttpRequester = new WeDoShoesCMSHttpRequester();
            _logger = LogManager.GetCurrentClassLogger();
        }
        internal List<TimeSlotView> ListTimeSlot()
        {
            List<TimeSlot> listTimeSlot = _weDoShoesCMSHttpRequester.ListTimeSlot();
            if (listTimeSlot == null)
                return null;
            List<TimeSlotView> listTimeSlotView = new List<TimeSlotView>();
            foreach (var timeSlot in listTimeSlot)
            {
                listTimeSlotView.Add(new TimeSlotView()
                {
                    Id = timeSlot.Id,
                    Name = timeSlot.Name
                });
            }

            return listTimeSlotView;
        }

        internal TimeSlotView TimeSlotById(int id)
        {
            TimeSlot timeSlot = _weDoShoesCMSHttpRequester.TimeSlotDetails(id);
            TimeSlotView timeSlotView = new TimeSlotView()
            {
                Id = timeSlot.Id,
                Name = timeSlot.Name
            };
            return timeSlotView;
        }

        internal bool CreateTimeSlot(TimeSlotView request)
        {
            TimeSlot timeSlot = new TimeSlot()
            {
                Name = request.Name
            };

            return _weDoShoesCMSHttpRequester.CreateTimeSlot(timeSlot);
        }

        internal bool UpdateTimeSlot(TimeSlotView request)
        {
            TimeSlot timeSlot = new TimeSlot()
            {
                Name = request.Name
            };

            return _weDoShoesCMSHttpRequester.UpdateTimeSlot(request.Id, timeSlot);            
        }

        internal bool DeleteTimeSlot(TimeSlotView request)
        {
            throw new NotImplementedException();
        }
    }
}
