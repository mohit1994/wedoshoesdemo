﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Util;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Services
{
    public class DefectService
    {
        private readonly WeDoShoesCMSHttpRequester _weDoShoesCMSHttpRequester;
        private readonly Logger _logger;

        public DefectService()
        {
            _weDoShoesCMSHttpRequester = new WeDoShoesCMSHttpRequester();
            _logger = LogManager.GetCurrentClassLogger();
        }

        internal List<DefectView> ListDefectView()
        {
            List<Defect> listDefect = _weDoShoesCMSHttpRequester.ListDefectView();
            if (listDefect == null)
                return null;
            List<DefectView> listDefectView = new List<DefectView>();
            foreach (var defect in listDefect)
            {
                listDefectView.Add(new DefectView()
                {
                    Id = defect.Id,
                    Name = defect.Name
                });
            }

            return listDefectView;
        }

        internal DefectView DefectById(int id)
        {
            Defect defect = _weDoShoesCMSHttpRequester.DefectDetails(id);
            DefectView defectView = new DefectView()
            {
                Id = defect.Id,
                Name = defect.Name
            };
            return defectView;
        }

        internal bool CreateDefect(DefectView request)
        {
            Defect defect = new Defect()
            {
                Name = request.Name
            };

            return _weDoShoesCMSHttpRequester.CreateDefect(defect);
        }

        internal bool UpdateDefect(DefectView request)
        {
            Defect defect = new Defect()
            {
                Name = request.Name
            };

            return _weDoShoesCMSHttpRequester.UpdateDefect(request.Id, defect);            
        }

        internal bool DeleteDefect(DefectView request)
        {
            throw new NotImplementedException();
        }

        internal List<DefectView> DefectByName(string keyword)
        {
            List<Defect> listDefect = _weDoShoesCMSHttpRequester.DefectByName(keyword);
            if (listDefect == null)
                return null;
            List<DefectView> listDefectView = new List<DefectView>();
            foreach (var defect in listDefect)
            {
                listDefectView.Add(new DefectView()
                {
                    Id = defect.Id,
                    Name = defect.Name
                });
            }

            return listDefectView;
        }
    }
}
