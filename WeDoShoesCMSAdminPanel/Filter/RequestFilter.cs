﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeDoShoesCMSAdminPanel.Services;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Filter
{
    public class RequestFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var _categoryService = new CategoryService();
            List<CategoryView> categories = _categoryService.GetCategories();
            if (categories==null)
            {
                filterContext.Controller.TempData["showWarning"] = true;
               // filterContext.HttpContext.Items["showWarning"] = true;
                filterContext.Result = new RedirectResult("~/Category/Create");
            }
            base.OnActionExecuting(filterContext);
        }
    }
}